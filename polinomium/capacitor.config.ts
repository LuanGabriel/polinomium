import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'polinomium',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
