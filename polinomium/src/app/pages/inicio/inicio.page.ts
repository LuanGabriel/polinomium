import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';
import { menuController } from '@ionic/core';
import { Licao } from 'src/app/models/licao';
import { MatriculaLicao } from 'src/app/models/matriculaLicao';
import { Usuario } from 'src/app/models/usuario';
import { LicaoService } from 'src/app/services/licao.service';
import { MatriculaLicaoService } from 'src/app/services/matricula-licao.service';
import { Color } from '../../utils/color';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  usuario:Usuario = new Usuario();
  licoesAtualizadas:boolean = false;
  fGroup: FormGroup;
  colors: string[] = [
    "#F8C93B", "#F8693F", "#F57075", "#684E9B"
  ]
  licoes: Licao[];
  constructor(private fBuilder: FormBuilder, public alertController:AlertController, private navController: NavController, private toastController:ToastController, public menuCtrl:MenuController, public licaoService:LicaoService, public matriculaLicaoService:MatriculaLicaoService) { 
    
  }
  
  ngOnInit() {
  }
    
  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'))
    this.menuCtrl.enable(true);
    this.licaoService.listarLicoes().then( json => {
      this.licoes = <Licao[]>(json);
      this.licoes.forEach(licao => {
        this.checarLicao(licao);
      });
      this.licoes[0].isLocked = true;
      this.licoesAtualizadas = true;
      //console.log(this.licoes)
    })
  }
  
  checarLicao(licaoHTML:Licao){
    let isLocked = 0;
    let licao:Licao = new Licao();
    this.licaoService.encontrarLicao(licaoHTML.id.toString()).then( json => {
      licao = <Licao>(json);
      // debugger;
      if(licao.matriculaLicao.length>0){
        licaoHTML.isLocked = true;
      }else{
        licaoHTML.isLocked = false;
      }
      
    })
    licao.isLocked = false;
  }

  
  getContrastColor(color:string){
    let rgbColor = Color.hexToRgb(color)
    return Color.contrastColor(rgbColor.r, rgbColor.g, rgbColor.b)
  }

  acessarLicao(id:number){
    for(let i = 0; i < this.licoes.length; i++){
      if(this.licoes[i].id === id){
        if(this.licoes[i].isLocked){
          this.navController.navigateBack('/licao/'+id);
        }else{
          this.licaoBloqueadaAlert();
        }
      }
    }
  }

  licaoBloqueadaAlert(){
    this.presentAlert('Calma lá!', 'Realize antes as lições desbloqueadas!')
  }

  async presentAlert(titulo:string, texto:string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: titulo,
      message: texto,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

}
