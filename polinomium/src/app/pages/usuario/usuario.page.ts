import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

  usuario = {
    nome:null,
    email:null,
    username:null,
    senha:null,
    dataNascimento:null,
    //licoes:[]
  }
  fGroup: FormGroup;

  constructor(private fBuilder:FormBuilder, public navController:NavController, public usuarioService:UsuarioService, public menuCtrl: MenuController) {
    this.fGroup = this.fBuilder.group({
      'nome': [this.usuario.nome, Validators.compose([
        Validators.required
      ])],
      'email': [this.usuario.email, Validators.compose([
        Validators.required,
        // this.checarEmail,
        // Validators.email
      ])],
      'senha': [this.usuario.senha, Validators.compose([
        Validators.required
      ])],
      'username': [this.usuario.username, Validators.compose([
        Validators.required
      ])],
      'dataNascimento': [this.usuario.dataNascimento, Validators.compose([
        Validators.required
      ])] 
    })
  }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    this.fGroup.get('nome').setValue(this.usuario.nome);
    this.fGroup.get('email').setValue(this.usuario.email);
    this.fGroup.get('dataNascimento').setValue(this.usuario.dataNascimento);
    this.fGroup.get('username').setValue(this.usuario.username);
    this.fGroup.get('senha').setValue(this.usuario.senha);

  }

  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

}