import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, MenuController, ModalController, NavController, ToastController } from '@ionic/angular';
import { Usuario } from 'src/app/models/usuario';
import { AmigoService } from 'src/app/services/amigo.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-amigos',
  templateUrl: './amigos.page.html',
  styleUrls: ['./amigos.page.scss'],
})
export class AmigosPage implements OnInit {
  email: string;
  usuariosSelecao: Array<Usuario>;
  amigos: Array<Usuario>;
  autenticado:Usuario;
  amigosAtualizados = false;

  constructor(public alertController:AlertController, public loadingController:LoadingController, public modalController: ModalController, public navController:NavController, public amigoService:AmigoService, public usuarioService:UsuarioService, public toastController:ToastController, public menuCtrl:MenuController) {
  }

  ngOnInit() {
    this.autenticado = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    this.usuarioService.encontrarAmigos(this.autenticado.id).then(json=>{
      this.amigos = <Usuario[]>(json);
      console.log(this.amigos)
      if(this.amigos.length>=0){
        this.usuariosSelecao = this.amigos;
        this.amigosAtualizados=true;
      }
    })

  }

  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

  async atualizarLista(){
    await this.delay(50);
    if(this.email===""){
        this.usuariosSelecao = this.amigos;
    }else{
      this.usuarioService.encontrarPorEmail(this.email, this.autenticado.id).then( json => {
        this.usuariosSelecao = new Array<Usuario>();
        if(json.name!="HttpErrorResponse"){
          this.usuariosSelecao[0] = <Usuario>(json);
        }else{
          this.usuariosSelecao = this.amigos;
        }
      })
    }
  }

  async adicionarAmigo(id:number){
    this.usuarioService.getById(id).then(async json=>{
      let amigo = <Usuario>(json);
      console.log(amigo)
      this.usuarioService.addAmigo(this.autenticado, amigo);
      this.presentLoading();
      await this.delay(2000);
      this.usuarioService.encontrarAmigos(this.autenticado.id).then(json=>{
        this.amigos = <Usuario[]>(json);
        console.log(this.amigos);
      })
      this.presentToast('success','Amigo adicionado!!!')
      this.atualizarLista();
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Por favor, aguarde...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  async presentToast(cor:any, msg:any) {
    const toast = await this.toastController.create({
      message: msg,
      color: cor,
      duration: 2000
    });
    toast.present();
  }

  async confirmarExclusao(id:number) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Você confirma a remoção do amigo?',
      message: 'Você poderá adicioná-lo novamente depois',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            console.log('Confirm Cancel: não excluir');
          }
        }, {
          text: 'Okay',
          id: 'confirm-button',
          handler: () => {
            this.removerAmigo(id);
          }
        }
      ]
    });

    await alert.present();
  }
  
  async removerAmigo(id:number){
    this.usuarioService.getById(id).then(async json=>{
      let amigo = <Usuario>(json);
      console.log(amigo)
      this.usuarioService.deleteAmigo(this.autenticado, amigo);
      this.presentLoading();
      await this.delay(2000);
      this.usuarioService.encontrarAmigos(this.autenticado.id).then(json=>{
        this.amigos = <Usuario[]>(json);
        console.log(this.amigos);
      })
      this.presentToast('success','Amigo removido!!!')
      this.atualizarLista();
    });
  }

}
