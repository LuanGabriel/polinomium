import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController } from '@ionic/angular';
import { Usuario } from 'src/app/models/usuario';
import { MatriculaLicaoService } from 'src/app/services/matricula-licao.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-add-usuario',
  templateUrl: './add-usuario.page.html',
  styleUrls: ['./add-usuario.page.scss'],
})
export class AddUsuarioPage implements OnInit {

  usuario : Usuario = new Usuario();
  fGroup: FormGroup;

  constructor(private fBuilder:FormBuilder, public navController:NavController, public usuarioService:UsuarioService, public matriculaLicaoService:MatriculaLicaoService, public menuCtrl: MenuController) {
    this.fGroup = this.fBuilder.group({
      'nome': [this.usuario.nome, Validators.compose([
        Validators.required
      ])],
      'email': [this.usuario.email, Validators.compose([
        Validators.required,
        // this.checarEmail,
        Validators.email
      ])],
      'senha': [this.usuario.senha, Validators.compose([
        Validators.required
      ])],
      'username': [this.usuario.username, Validators.compose([
        Validators.required
        // this.checarUsername
      ])],
      'dataNascimento': [this.usuario.dataNascimento, Validators.compose([
        Validators.required
      ])] 
    })
  }

  ngOnInit() {
     this.menuCtrl.enable(false);
  }

  // // checarUsername(control: FormControl){
  // //   let usuarioservice = new UsuarioService();
  // //   let teste = usuarioservice.checarUsername(control.value);

  // //   if(teste){
  // //     return{
  // //       userDomain: {
  // //         parsedDomain: false
  // //       }
  // //     }
  // //   }else{
  // //     return null;
  // //   }

  // }

  async submitForm(){
    this.usuario.nome = this.fGroup.get('nome').value;
    this.usuario.email = this.fGroup.get('email').value;
    this.usuario.username = this.fGroup.get('username').value;
    this.usuario.senha = this.fGroup.get('senha').value;
    this.usuario.dataNascimento = this.fGroup.get('dataNascimento').value;
    console.log(this.usuario)
    let user = this.usuarioService.inserirUsuario(this.usuario);
    this.matriculaLicaoService.introduzir(await user);
    // this.usuarioService.inserirUsuario(this.usuario);
    this.navController.navigateBack("/login");
  }

  cancelar(){
    this.fGroup.get('nome').setValue(null);
    this.fGroup.get('email').setValue(null);
    this.fGroup.get('username').setValue(null);
    this.fGroup.get('senha').setValue(null);
    this.fGroup.get('dataNascimento').setValue(null);
  }

}