import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  fGroup:FormGroup;
  usuario: Usuario = new Usuario();

  constructor(public app:AppComponent, private fBuilder:FormBuilder, public navController:NavController, public usuarioService:UsuarioService, public toastController:ToastController, public menuCtrl:MenuController) {
    this.fGroup = this.fBuilder.group({
      'senha': [this.usuario.senha, Validators.compose([
        Validators.required
      ])],
      'username': [this.usuario.username, Validators.compose([
        Validators.required
      ])] 
    })
  }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.menuCtrl.enable(false);
  }

  autenticar(){
    this.usuario.senha = this.fGroup.get('senha').value;
    this.usuario.username = this.fGroup.get('username').value;
    console.log(this.usuario)
    this.usuarioService.autenticar(this.usuario).subscribe((data) => {
      let usuarioAutenticado = <Usuario>(data);
      console.log(usuarioAutenticado)
      if(usuarioAutenticado != null){
        if(usuarioAutenticado.userAtivo){
          this.presentToast("primary","Usuário autenticado!");
          localStorage.setItem('usuarioAutenticado', JSON.stringify(usuarioAutenticado));
          this.app.atualizarUser();
          this.navController.navigateBack('/inicio');
        }else{
          this.presentToast("danger", "Dados incorretos! Tente de novo")
          this.cancelar();
        }
      }else{
        this.presentToast("danger", "Dados incorretos! Tente de novo")
        this.cancelar();
      }
    }, erro => {
      this.presentToast("danger", "Dados incorretos! Tente de novo")
      this.cancelar();
    });

  }

  cancelar(){
    this.fGroup.get('senha').setValue(null);
    this.fGroup.get('username').setValue(null);
  }

  async presentToast(cor:any, msg:any) {
    const toast = await this.toastController.create({
      message: msg,
      color: cor,
      duration: 2000
    });
    toast.present();
  }


}