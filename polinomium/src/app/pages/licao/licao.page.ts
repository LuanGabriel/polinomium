import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Licao } from 'src/app/models/licao';
import { LicaoService } from 'src/app/services/licao.service';

@Component({
  selector: 'app-licao',
  templateUrl: './licao.page.html',
  styleUrls: ['./licao.page.scss'],
})
export class LicaoPage implements OnInit {
  
  paragrafos:string[] = new Array;
  licao: Licao;
  licaoChegou:boolean=false;
  id:string;
  licaoId:string
  tiposdeTexto:string[] = new Array;
  tiposdeTextoCarregou : boolean = false;

  constructor(public activatedRoute: ActivatedRoute, public navController:NavController, public licaoService:LicaoService) { 
    this.licaoId = "licao/"+this.id;
  }
  
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    console.log(this.paragrafos)
    this.licaoService.encontrarLicao(this.id).then( json => {
      this.licao = <Licao>(json);
      this.licaoChegou = true;
      let split =this.licao.conteudo.split('\n')
      split.forEach((item) => {this.paragrafos.push(item)})
      this.criarTiposTexto();
      this.licaoId = "/licao/"+this.id;
    })
  }

  criarTiposTexto(){
    for(let i = 0; i < this.paragrafos.length; i++){
      if(this.paragrafos[i].includes('formula')){
        this.tiposdeTexto.push('formula');
        this.paragrafos[i] = this.paragrafos[i].slice(9,this.paragrafos[i].length-10)
      }else{
        this.tiposdeTexto.push('texto')
      }
    }
    console.log(this.paragrafos)
    this.tiposdeTextoCarregou = true;
  }


  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

  async exercitar(id:number){
    this.navController.navigateBack("/exercicio/"+id);
  }

}
