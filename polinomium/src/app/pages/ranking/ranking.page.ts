import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Licao } from 'src/app/models/licao';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.page.html',
  styleUrls: ['./ranking.page.scss'],
})
export class rankingPage implements OnInit {

  posicao: any[]; 
  rankingFinal: number;
  usuarios: any[]; 
  usuario:Usuario;

  constructor(public activatedRoute: ActivatedRoute, public navController:NavController, public usuarioService:UsuarioService) { 
  
  }
  
  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    this.usuarioService.getById(this.usuario.id).then(json =>{
      this.usuario = <Usuario>(json);
      localStorage.setItem('usuarioAutenticado',JSON.stringify(this.usuario));
      // this.posicao = 8;
    });
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    this.descobrirRanking();

  }
  
  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

  
  descobrirRanking(){
    this.usuarioService.listarUsuarios().then( json => {
      this.usuarios = <Usuario[]>(json);
      //console.log(this.licoes)
      
      
      console.log(this.usuarios)
      this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
      console.log(this.usuario);
      let aux= 0;
      for(let i=0; i<this.usuarios.length;i++){
        for(let a=0; a<this.usuarios.length;a++){
          if(this.usuarios[i].xp>this.usuarios[a].xp){
            aux=this.usuarios[i];
            this.usuarios[i]=this.usuarios[a];
            this.usuarios[a]=aux;
          }
        } 
      }
      
      for(let i=0; i<this.usuarios.length;i++){
        if(this.usuario.id === this.usuarios[i].id){
          this.rankingFinal=i+1;
        }
      }
    });
  }


  ionViewWillEnter(){
    
  }

  async exercitar(id:number){
    
  }

}
