import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { rankingPage } from './ranking.page';

const routes: Routes = [
  {
    path: '',
    component: rankingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class rankingPageRoutingModule {}
