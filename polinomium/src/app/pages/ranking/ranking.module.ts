import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { rankingPageRoutingModule } from './ranking-routing.module';

import { rankingPage } from './ranking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    rankingPageRoutingModule
  ],
  declarations: [rankingPage]
})
export class rankingPageModule {}
