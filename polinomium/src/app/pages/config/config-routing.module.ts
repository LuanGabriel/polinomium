import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { configPage } from './config.page';

const routes: Routes = [
  {
    path: '',
    component: configPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class configPageRoutingModule {}
