import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { configPageRoutingModule } from './config-routing.module';

import { configPage } from './config.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    configPageRoutingModule
  ],
  declarations: [configPage]
})
export class configPageModule {}
