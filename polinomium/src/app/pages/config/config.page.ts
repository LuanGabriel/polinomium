import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  {AlertController, NavController } from '@ionic/angular';
import { Licao } from 'src/app/models/licao';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
})
export class configPage implements OnInit {

  usuario: Usuario;

  constructor(public alertController:AlertController, public activatedRoute: ActivatedRoute, public navController:NavController, public usuarioService:UsuarioService) { 
  
  }
  
  ngOnInit() {
    this.usuario= JSON.parse(localStorage.getItem('usuarioAutenticado'));
  }


  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

  async excluir(){
    // console.log("sdasdasdas");
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Atenção!',
      message: 'Deseja <strong>deletar sua conta para sempre</strong>???',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sim!',
          id: 'confirm-button',
          handler: () => {
            this.excluirUser();
          }
        }
      ]
    });
    await alert.present();
  }
 
  excluirUser(){
    this.usuario.userAtivo = false;
    this.usuarioService.updateUsuario(this.usuario);
    localStorage.removeItem('usuarioAutenticado'); 
    this.navController.navigateBack('/login')
  }

}
