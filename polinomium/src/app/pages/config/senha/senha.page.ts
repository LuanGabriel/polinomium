import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { AlertController ,NavController, ToastController } from '@ionic/angular';
import { FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Licao } from 'src/app/models/licao';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'senha',
  templateUrl: './senha.page.html',
  styleUrls: ['./senha.page.scss'],
})
export class senhaPage implements OnInit {

  fGroup: FormGroup;
  usuario: Usuario;
  senhaAtual = null;
  novaSenha = null; 
  confirmNovaSenha = null;
  senhaCompare=null;
  constructor(public router:Router, public toastController:ToastController, public alertController:AlertController, private fBuilder: FormBuilder, public activatedRoute: ActivatedRoute, public navController:NavController, public usuarioService:UsuarioService) { 
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    this.fGroup = this.fBuilder.group({
      'senhaAtual': [this.senhaAtual, Validators.compose([
        Validators.required
      ])],
      'novaSenha': [this.novaSenha, Validators.compose([
        Validators.required
      ])],
      'confirmNovaSenha': [this.confirmNovaSenha, Validators.compose([
        Validators.required
      ])]
    })
  }
  
  ngOnInit() {
    
  }


  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

  async submitForm(){
    this.senhaAtual=this.fGroup.get('senhaAtual').value;
    this.confirmNovaSenha=this.fGroup.get('confirmNovaSenha').value;
    this.novaSenha=this.fGroup.get('novaSenha').value;
  

    if(!(this.confirmNovaSenha===this.novaSenha)){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'As novas senhas digitadas não coincidem',
      message: 'Digite as senhas novamente',
      buttons: [
       {
          text: 'Okay',
          id: 'confirm-button',
          handler: () => {
           
          }
        }
      ]
    });

    await alert.present();
    }else if(this.senhaAtual !== this.usuario.senha){
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'A senha digitada não coincide com a atual',
        message: 'Digite a senha novamente',
        buttons: [
         {
            text: 'Okay',
            id: 'confirm-button',
            handler: () => {
             
            }
          }
        ]
      });
  
      await alert.present();
    }else{
      this.mudarSenha();
    }

    
  }

  mudarSenha(){
    this.usuario.senha= this.novaSenha;
    this.usuarioService.updateUsuario(this.usuario);
    localStorage.setItem('usuarioAutenticado', JSON.stringify(this.usuario));
    this.router.navigate(['/config']).then(() => {window.location.reload();});  
  }

  
    
}
