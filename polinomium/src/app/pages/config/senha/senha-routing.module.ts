import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { senhaPage } from './senha.page';

const routes: Routes = [
  {
    path: '',
    component: senhaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class senhaPageRoutingModule {}
