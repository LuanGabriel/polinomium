import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { senhaPageRoutingModule } from './senha-routing.module';

import { senhaPage } from './senha.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    senhaPageRoutingModule,
    ReactiveFormsModule

  ],
  declarations: [senhaPage]
})
export class senhaPageModule {}
