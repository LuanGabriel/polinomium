import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, IonSlide, NavController, ToastController } from '@ionic/angular';
import { alertController } from '@ionic/core';
import { Exercicio } from 'src/app/models/exercicio';
import { Licao } from 'src/app/models/licao';
import { Usuario } from 'src/app/models/usuario';
import { ExercicioService } from 'src/app/services/exercicio.service';
import { MatriculaLicaoService } from 'src/app/services/matricula-licao.service';
import { LicaoService } from 'src/app/services/licao.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-exercicio',
  templateUrl: './exercicio.page.html',
  styleUrls: ['./exercicio.page.scss'],
})
export class ExercicioPage implements OnInit {
  
  idLicao:string;
  exercicios: Exercicio[];
  exerciciosChegou: boolean;
  usuario:Usuario;
  licoes: Licao[];

  constructor(public navController:NavController, public router:Router, public activatedRoute: ActivatedRoute, public exercicioService:ExercicioService, public toastController:ToastController, public alertController:AlertController, public matriculaLicaoService:MatriculaLicaoService, public licaoService:LicaoService, public usuarioService:UsuarioService) { }

  ngOnInit() {
    this.idLicao = this.activatedRoute.snapshot.paramMap.get('id');
    this.exercicioService.encontrarPorLicao(Number(this.idLicao)).then( json => {
      this.exercicios = <Exercicio[]>(json);
      this.exercicios.forEach(element => {
        element.acertou = false;
      });
    })
    this.licaoService.listarLicoes().then( json => {
      this.licoes = <Licao[]>(json);
    })
  }

  async ionViewWillEnter(){
    if(JSON.parse(localStorage.getItem('usuarioAutenticado')) === null){
      this.navController.navigateBack('/login')
    }
  }

  async clicar(exercicio: Exercicio, valor:string){
    if(exercicio.solucao != valor){
      this.presentToast("danger","Você errou! Tente de novo")
      this.exercicios.find(x => x == exercicio).acertou = false;
    }else{
      this.presentToast("primary","Boaa!!!")
      this.exercicios.find(x => x == exercicio).acertou = true;
      if(this.finalizar()){
        // console.log(this.exercicios)
        this.finalizarAlert();
      }
    }
  }

  async finalizarAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Atenção!',
      message: 'Deseja <strong>finalizar a lição</strong>???',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          id: 'cancel-button',
          handler: (blah) => {
            // console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sim!',
          id: 'confirm-button',
          handler: () => {
            this.adicionarXp();
            this.finalizarLicao();
          }
        }
      ]
    });

    await alert.present();
  }


  finalizarLicao(){
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    // let idUsuario:string = this.usuario.id.toString();
    this.matriculaLicaoService.addMatriculaLicao(Number(this.usuario.id),Number(this.idLicao));
    this.usuarioService.registrarOfensiva();
    // this.adicionarXp();
    // this.navController.navigateBack("/inicio");
    this.router.navigate(['/inicio']).then(() => {window.location.reload();});
  }

  adicionarXp(){
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    //console.log(this.licoes);
    this.usuario.xp= this.usuario.xp+this.licoes[parseInt(this.idLicao)-1].recompensa;
    
    this.usuarioService.updateUsuario(this.usuario);
   

  }


  voltarLicao(){
    this.router.navigate(['/licao/'+this.idLicao]).then(() => {window.location.reload();});
  }

  async presentToast(cor:any, msg:any) {
    const toast = await this.toastController.create({
      message: msg,
      color: cor,
      duration: 2000
    });
    toast.present();
  }

  finalizar(){
    let certos = 0;
    for(let i = 0; i < this.exercicios.length; i++){
      if(this.exercicios[i].acertou){
        certos++;
      }
    }
    if(certos===this.exercicios.length){
      return true;
    }
  }



}
