import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Usuario } from './models/usuario';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Sobre mim', url: 'usuario', icon: 'person' },
    { title: 'Amigos', url:'amigos', icon:'people'},
    { title: 'Lição', url:'inicio', icon:'book'},
    { title: 'Ranking', url:'ranking', icon:'trophy'},
    { title: 'Configurações', url:'config', icon:'settings'}
  ];

  usuario:Usuario;
  nome:string;
  email:string;

  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(public navController:NavController) {
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    if(this.usuario!=null){
      this.nome = this.usuario.nome.replace(/ .*/,'');
      this.email = this.usuario.email;
    }
  }

  atualizarUser(){
    this.usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    this.nome = this.usuario.nome.replace(/ .*/,'');
    this.email = this.usuario.email;
  }

  logout(){
    localStorage.removeItem('usuarioAutenticado');
    this.navController.navigateBack('/login')
  }

}
