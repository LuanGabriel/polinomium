export class Usuario{
    id:number;
    nome:string;
    dataNascimento: Date;
    userAtivo: boolean;
    username: string;
    senha:string;
    xp:number;
    ofensiva:Array<Date>;
    nivel:number;
    email:string;
    amigos:Array<Usuario>;
}