import { MatriculaLicao } from "./matriculaLicao";

export class Licao{
    
    id:number;
    conteudo:string;
    nivel:number;
    nome:string;
    recompensa:number;
    isLocked:boolean = true;
    isFinished:boolean = false;
    matriculaLicao: MatriculaLicao[]
}