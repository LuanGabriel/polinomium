import { Licao } from "./licao";

export class Exercicio{
    id:number;
    a:string;
    b:string;
    c:string;
    d:string;
    dificuldade:number;
    conteudo:string;
    enunciado:string;
    solucao:string;
    licao:Licao;
    acertou:boolean = false;
}