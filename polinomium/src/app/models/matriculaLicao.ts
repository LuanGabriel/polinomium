import { Licao } from "./licao";
import { Usuario } from "./usuario";

export class MatriculaLicao{
    id:number;
    isFinished:boolean;
    isLocked:boolean;
    licao:Licao;
    usuario:Usuario
}