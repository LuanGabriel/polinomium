import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { stringify } from 'querystring';
import { Usuario } from '../models/usuario';
import { UsuarioPageRoutingModule } from '../pages/usuario/usuario-routing.module';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  
  urlServidor: string = "http://localhost:8080/usuarios/";
  
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  
  constructor(private httpClient:HttpClient) { }

  async listarUsuarios(){
    
    try{
      let json = await this.httpClient.get(this.urlServidor).toPromise();
      return json;
    }catch(erro){
      return erro;
    }
  }
  
  async registrarOfensiva() {
    let now = new Date();
    let usuario = JSON.parse(localStorage.getItem('usuarioAutenticado'));
    let ultimaEntrada = new Date(usuario.ofensiva[usuario.ofensiva.length-1]);
    if(ultimaEntrada.getDay()===now.getDay()-1){
      usuario.ofensiva.push(now)
      this.updateUsuario(usuario)
      localStorage.setItem('usuarioAutenticado', JSON.stringify(usuario));
    }
  }
  
  async encontrarAmigos(id:number){
    let url = this.urlServidor + id + "/amigos";
    try{
      let json = await this.httpClient.get(url).toPromise();
      return json;
      // console.log(json);
    } catch(erro){
      return erro;
    }
  }

  async deleteAmigo(usuario:Usuario, amigo:Usuario){
    let url = this.urlServidor + usuario.id +"/"+amigo.id;
    try{
      let json = await this.httpClient.delete(url).toPromise();
    }catch(erro){
      return erro;
    }
  }

  async deleteUsuario(usuario:Usuario){
    let url = this.urlServidor + usuario.id;
    try{
      let json = await this.httpClient.delete(url).toPromise();
      return json;
    }catch(erro){
      return erro;
    }
  }

  autenticar(usuario: Usuario){
    let url = this.urlServidor + usuario.username + "/" + usuario.senha + "/exists";
    console.log(url) 
    return this.httpClient.get(url)
      
  }



  async encontrarPorEmail(email:string, id:number){
    let url = this.urlServidor + "email/" + email + "/id/" + id;
    try{
      let json = await this.httpClient.get(url).toPromise();
      return json;
      // console.log(json);
    } catch(erro){
      return erro;
    }
  }

  async inserirUsuario(usuario: Usuario){

    try{
      usuario.nivel = 1;
      usuario.ofensiva = new Array<Date>();
      usuario.ofensiva.push(new Date());
      usuario.xp = 0;
      console.log(usuario)
      let json = await this.httpClient.post(this.urlServidor, JSON.stringify(usuario), this.httpOptions).toPromise();
      return json;
    }catch(erro){
      return erro;
    }

  }

  async updateUsuario(usuario: Usuario){
    try{
      let json = await this.httpClient.put(this.urlServidor, JSON.stringify(usuario), this.httpOptions).toPromise();
      return json;
    }catch(erro){
      return erro;
    }
  }

  // checarUsername(username:any){
  //   let usuarios = this.listarUsuarios();

  //   for(let i = 0; i < usuarios.length; i++){
  //     if(usuarios[i].username===username){
  //       return true;
  //       // encontrou igual
  //     }
  //   }

  //   return false;
  //   // não encontrou
  // }
  async getById(id:number){
    let url = this.urlServidor + id;
    try{
      console.log(url)
      let json = await this.httpClient.get(url).toPromise();
      console.log(json);
      return json;
    } catch(erro){
      return erro;
    }
  }

  async addAmigo(usuario:Usuario, amigo:Usuario){
    let url = this.urlServidor + usuario.id + "/addAmigo/" + amigo.id;
    console.log(url)
    try{
      let json = await this.httpClient.post(url, this.httpOptions).toPromise();
      console.log(json)
      return json;
    }catch(erro){
      return erro;
    }
  }


}
