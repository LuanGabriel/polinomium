import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExercicioService {

  
  urlServidor: string = "http://localhost:8080/atividades/";
  
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  
  constructor(private httpClient:HttpClient) { }

  async encontrarPorLicao(id:number){

    let url = this.urlServidor + "getByLicao/" + id;

    try{
      let json = await this.httpClient.get(url).toPromise();
      console.log(json)
      return json;
    }catch(erro){
      return erro;
    }
  }
}