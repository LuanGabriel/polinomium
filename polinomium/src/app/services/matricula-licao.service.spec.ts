import { TestBed } from '@angular/core/testing';

import { MatriculaLicaoService } from './matricula-licao.service';

describe('MatriculaLicaoService', () => {
  let service: MatriculaLicaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatriculaLicaoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
