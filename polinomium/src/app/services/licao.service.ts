import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LicaoService {

  urlServidor: string = "http://localhost:8080/licoes/";

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };


  constructor(private httpClient:HttpClient) { }

  async listarLicoes(){

    try{
      let json = await this.httpClient.get(this.urlServidor).toPromise();
      return json;
    }catch(erro){
      return erro;
    }

  }

  async encontrarLicao(id:string) {
    let url = this.urlServidor + id;
    try{
      let json = await this.httpClient.get(url).toPromise();
      return json;
    }catch(erro){
      return erro;
    }

  }
}
