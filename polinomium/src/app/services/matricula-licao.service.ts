import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario } from '../models/usuario';


@Injectable({
  providedIn: 'root'
})
export class MatriculaLicaoService {

  urlServidor: string = "http://localhost:8080/matriculaLicoes/";

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };


  constructor(private httpClient:HttpClient) { }

  async addMatriculaLicao(idUsuario:number, idLicao:number){
    try{
      let url = this.urlServidor + idUsuario + "/" + idLicao
      console.log(url)
      let json = await this.httpClient.post(url, this.httpOptions).toPromise();
      return json;
    }catch(erro){
      return erro;
    }
  }

  async getByUsuario(idLicao:number, idUsuario:number){
    let url = this.urlServidor + idLicao + "/" + idUsuario;
    try{
      let json = await this.httpClient.get(url).toPromise();
      return json;
    }catch(erro){
      return erro;
    }
  }

  async introduzir(usuario:Usuario){
    try{
      let url = this.urlServidor + "introduzir/" + usuario.id 
      // console.log(url)
      let json = await this.httpClient.post(url, this.httpOptions).toPromise();
      return json;
    }catch(erro){
      return erro;
    }
  }

}
